/*
  ligne de commande pour compiler: g++ tp1.cpp -o tp1 -lm -std=c++11
  ligne de commande pour exécuter: ./tp1 -s test.txt
  
  Programme ecrit par Lilly-Gabrielle Champagne [CHAL06589118] et Emilie Paquin [PAQE19528909]
  Dans le cadre du cours INF3105 - Premier TP, correction de mots

  */
#include <fstream>
#include <iostream>
#include <cctype>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
using namespace std;
 
void rechercheMotAlgo2(const string motTemp, vector<string> &listeMots, vector<string> &sortie);
void rechercheLettreAlgo1(const string motTemp, vector<string> &dicto, vector<string> &sortie, int indexDebut, int indexFin, int &min);
void rechercheDictioAlgo1(const string motTemp, vector<string> &dicto, vector<string> &sortie, int &min);
void formatage(const string motPrecedent, string &motSortie);
int distance(string const enRecherche, string const test);
string motMinuscule(string phrase);
bool entreeStandard(string &nomFichier, int taille, char * tabEntree[]);
int dist(string motDict, string motTemp);
int minimum(int a, int b);
string alphabetique(const string motATrier);

/*methode pour recuperer les informations necessaires sur lentree standard (-s et nom du fichier a ouvrir)*/
bool entreeStandard(string &nomFichier, int taille, char * tabEntree[]);
int main(int argc, char * argv[]){

/*variable qui prend en parametre le fichier contenant la
liste des mots à corriger.*/
string precedent="premier", contenu,tempo, nomFichier, motTemp, motDict, motCorrige, sortie, motTest;
bool etatS=entreeStandard(nomFichier, argc, argv);

/*variables pour utiliser les mots*/
char lettre, caractere;
int indexFin, indexDebut, temp, min, premier=0;
bool verifierCarac(char caractere);

/*conteneur pour le dictionnaire*/
vector<string> dicto;
vector<string> aCorriger;
vector<string> motsValides;
vector<string> motsValidesOption;
vector<string>::iterator it, itd, its;

/*initialisation de la position des lettres dans le dictionnaire*/
map<char,int> debut={{'a', 45},{'b', 23845},{'c', 41592},{'d', 72205},
{'e', 90467},{'f', 104085},{'g', 115755},{'h', 126062},{'i', 139053},
{'j', 152029},{'k', 154598},{'l', 158149},{'m', 167452},{'n', 186005},
{'o', 199024},{'p', 211301},{'q', 244641},{'r', 246392},{'s', 262830},
{'t', 300269},{'u', 318061},{'v', 340709},{'w', 345863},{'x', 352305},
{'y', 352735},{'z', 353777}};
ifstream fichier("words.txt", ios::in);
ifstream corrige(nomFichier.c_str(), ios::in);
  if(fichier && corrige){
    do{
      corrige.get(caractere);
      if(!isalnum(caractere) & caractere != '\''){
        if(tempo.size() != 0){
          aCorriger.push_back(tempo);
          tempo.clear();
        }
        motTemp=(1,caractere);
        aCorriger.push_back(motTemp);
      }else{
        tempo=tempo+""+caractere;
      }
    }while(!corrige.eof());
      while(getline(fichier,contenu)){
        dicto.push_back(contenu);
      }

      /*Appel de la fonction pour mettre les mots en minuscules*/
      for(it = aCorriger.begin(); it != aCorriger.end();++it){
        tempo = *it;
        motsValides.clear();
        min = 99;
        if(isalnum(tempo[0])){
          motTemp = motMinuscule(tempo); 
          caractere= motTemp[0];
            
          //trouver lindex de la lettre a utiliser
          indexDebut = debut[caractere];  
          indexFin = debut[++caractere];  
        
          //acceder au mot du dictionnaire via recherche binaire:
          if (binary_search(dicto.begin(), dicto.end(), motTemp)){
                
            //si mot trouve, on le motre a l'ecran  
            formatage(precedent, tempo);
            precedent=tempo;
            cout << tempo;
          } else {
                
            //si le mot n'est pas trouve, debuter l'algorithme de correction en passant chaque mot du dictionnaire valide.
            rechercheLettreAlgo1(motTemp, dicto, motsValides, indexDebut, indexFin, min);
            if (min > 1){
                    
              //Si on ne trouve pas un mot avec une distance de 1, on cherche le dictionnaire au complet
              rechercheDictioAlgo1(motTemp, dicto, motsValides, min);     
            }if(!etatS){
              sortie=motsValides.front();
                  
              //Afficher le mot corrige
              formatage(precedent, sortie);
              precedent=tempo;
              cout<<sortie; 
            }else {
              rechercheMotAlgo2(motTemp, motsValides, motsValidesOption);
              motTest=motsValidesOption.front();
              formatage(precedent, motTest);
              precedent=tempo;
              cout<<motTest;
            } 
                  
          } 
          }else{
            precedent=tempo;
            cout << tempo;
          }
        }
  
        cout<<endl;
        fichier.close();
      }else{
        cerr<<"Impossible d'ouvrir le fichier !"<<endl;
      }
    return 0;
  }
/*
  Recherche du mot dans le vector contenant les mots avec la plus petites distances
*/
void rechercheMotAlgo2(const string motTemp, vector<string> &listeMots, vector<string> &sortie){
  int min=99, distanceAlgo2;
  string motTest;
  vector<string>::iterator itd;
  for(itd = listeMots.begin(); itd != listeMots.end(); itd++){
    motTest=*itd;
    distanceAlgo2=distance(motTemp,motTest);
    if(distanceAlgo2<min){
      if(!sortie.empty()){
        sortie.clear();
      }
      sortie.push_back(motTest);
      min=distanceAlgo2;
    }else if(distanceAlgo2 == min){
      sortie.push_back(motTest);
    }
  }
}
/*
  Recherche dans le dictionnaire a partir de la premiere lettre: Algorithme 1
*/
void rechercheLettreAlgo1(const string motTemp, vector<string> &dicto, vector<string> &sortie, int indexDebut, int indexFin, int &min){
  int temp;
    vector<string>::iterator itd;
    string motDict;
    for (itd = dicto.begin()+indexDebut; itd != dicto.begin()+indexFin; itd++){
      motDict = *itd;
      temp = dist(motDict, motTemp);
        
      //conserver les mots qui ont une moins grande distance
      if (temp < min) {
        sortie.clear();
        sortie.push_back(motDict);
        min = temp;
      }else if (temp == min){
        sortie.push_back(motDict);
      }              
    }
}
/*
  Recherche dans tout le dictionnaire: Algorithme 1
*/
void rechercheDictioAlgo1(const string motTemp, vector<string> &dicto, vector<string> &sortie, int &min){
  int temp=min;
    vector<string>::iterator itd;
    string motDict;
    for (itd = dicto.begin(); itd != dicto.end(); itd++){
      motDict = *itd;
      temp = dist(motDict, motTemp);
          
      //conserver les mots qui ont une moins grande distance
      if (temp < min) {
        if(!sortie.empty()){
          sortie.clear();
        }
        sortie.push_back(motDict);
        min = temp;
      }else if (temp == min){
        sortie.push_back(motDict);
      }
    }         
 }
/*
  Méthode pour formater le texte avant de l'afficher
*/
void formatage(const string motPrecedent, string &motSortie){
  int compteur=0;
  for(int i=0; i<motSortie.size(); i++){
    if(isupper(motSortie[i])){
      ++compteur;
    }
  }
  if(compteur!=motSortie.size() & compteur>1){
    for(int i=0; i<motSortie.size();i++){
      motSortie[i]=tolower(motSortie[i]);
    }
  }
  if(islower(motSortie[0]) && (motPrecedent == "?" ||motPrecedent == "!"||motPrecedent == "\n" ||motPrecedent == "." || motPrecedent == "premier")){
    motSortie[0]=toupper(motSortie[0]);
  }
}
/*
Algorithme 2 de distance
*/
int distance(string const enRecherche, string const test){
  map<char, int> alphabet1;
  map<char, int> alphabet2;
  vector<char> tab={'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
  int i=0;
  int taille=26;
  int retour=0;
  while(i<enRecherche.size()){
    ++alphabet1[enRecherche[i]];
    i++;
  }
  i=0;
  while(i<test.size()){
    ++alphabet2[test[i]];
    i++;
  }
  for(int i=0; i<taille;i++){
    if(alphabet1[tab[i]]!=alphabet2[tab[i]]){
      ++retour;
    }
  }
  return retour;
}
  
  /*
    Méthode pour trouver le nom du fichier et l'état (-s ou non) de l'entrée standard 
  */
  bool entreeStandard(string &nomFichier, int taille, char * tabEntree[]){
    bool retour = false;
    int i;
    int grandeur;
    string phrase;
    for(i=0;i<taille;i++){
      phrase=tabEntree[i];
      grandeur=phrase.size();
      if(phrase=="-s"){
        retour=true;
      }else if((phrase.substr(grandeur-3, grandeur))=="txt"){
        nomFichier=phrase;
      }
    }
    return retour;
  }
  /*
    Méthode pour que chaque mot soit en minuscule (On peut appeler notre fonction pour trouver les erreurs à partir de cette méthode pour que la recherche soit appliquer
    sur la string temporairement minuscule)
  */
  string motMinuscule(string phrase){
    string temporaire = phrase;
    for(int i = 0; i<temporaire.length(); i++){
      temporaire[i] = tolower(phrase[i]);
    }
    return temporaire;
  }
  
  
  /*
  Méthode qui retourne la distance entre le mot a valider et celui du dictionnaire
  Prend en parametre les deux mots ainsi qu'un minimum.
  */
  
  int dist(string motDict, string motTemp){
  
    int temp, compte, ret;
    int long1 = motTemp.length();
    int long2 = motDict.length();
    int matrice[long1 + 1][long2 + 1];
  
    if (long1 == 0){
      ret = long2;
    }
    if (long2 == 0){
      ret = long1;
    }
    //creation de la matrice en fonction des mots
    for (int i = 0; i <= long1; i++){
      matrice[i][0] = i;
    }
    for (int j = 0; j <= long2; j++){
      matrice[0][j] = j;
    }
    //trouver le minimum distance
    for (int i = 1; i<= long1; i++){
      for (int j = 1; j<= long2; j++){
        compte = (motTemp[j-1] == motDict[i-1]) ? 0 : 1;
        temp = minimum(matrice[i-1][j+1], matrice[i][j-1] + 1);
        matrice[i][j] = minimum(temp, matrice[i-1][j-1] + compte);
      }
    }
    
    if ((ret != long1) || (ret != long2)){
      ret = matrice[long1][long2];
    }
  
    return ret;
    
  }
  
  
  /*
  Methode qui compare 2 int et retourne le plus petit des deux
  */
  int minimum(int a, int b){
  
    int ret;
  
    if (a < b){
      ret = a;
    }
    else {
      ret = b;
    }
  
    return ret;
  
  }
  

  
